//
//  ObservedObject_PublishedApp.swift
//  ObservedObject_Published
//
//  Created by Artyom Romanchuk on 05.01.2021.
//

import SwiftUI

@main
struct ObservedObject_PublishedApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
